<?php
include('connect.php');

$postdata = file_get_contents("php://input");
$request = json_decode($postdata);

$id = $request->id;
$nama = $request->namaKegiatan;
$flagPatungan = $request->flagPatungan;
$tanggalKegiatan = date('Y-m-d', strtotime($request->tanggalKegiatan));

$query = "UPDATE m_kegiatan SET nama='$nama', tanggal_kegiatan='$tanggalKegiatan', flag_patungan='$flagPatungan' WHERE id='$id'";
$result = mysqli_query($conn, $query);

if ($result) {
    $response = array('data' => null, 'status' => 'SUCCESS', 'message' => 'Data berhasil diubah');
} else {
    $response = array('data' => null, 'status' => 'ERROR', 'message' => 'Gagal menambahkan data ke database');
}

header('Content-Type: application/json');
echo json_encode($response);
