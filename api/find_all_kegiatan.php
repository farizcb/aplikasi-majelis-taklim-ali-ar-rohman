<?php
include('connect.php');

$query = "SELECT * FROM `m_kegiatan` ORDER BY tanggal_dibuat DESC";

$result = mysqli_query($conn, $query);
if (!$result) {
    printf("Error: %s\n", mysqli_error($conn));
    exit();
}

if (mysqli_num_rows($result) != 0) {
    while ($row = mysqli_fetch_assoc($result)) {
        $dataArray[] = $row;
    }
    echo json_encode($dataArray);
}
