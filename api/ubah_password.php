<?php

include('connect.php');

$postdata = file_get_contents("php://input");
$request = json_decode($postdata);

$email = $request->email;
$password = $request->password;

$hash = password_hash($password, PASSWORD_BCRYPT);

$query_find = "SELECT * FROM c_security_user WHERE email = '$email'";
$result = mysqli_query($conn, $query_find);

if (!$result) {
    printf("Error: %s\n", mysqli_error($conn));
    exit();
}

if (mysqli_num_rows($result) != 0) {
    $dataUser = mysqli_fetch_assoc($result);
    $id = $dataUser["id"];
    $query_update = "UPDATE c_security_user SET password = '$hash' WHERE id = '$id'";
    $result_update = mysqli_query($conn, $query_update);
    if ($result_update) {
        $response = array('data' => null, 'status' => 'SUCCESS', 'message' => 'Password berhasil diubah, silahkan login kembali !');
    } else {
        $response = array('data' => null, 'status' => 'ERROR', 'message' => 'Gagal mengubah password !');
    }
}

header('Content-Type: application/json');
echo json_encode($response);