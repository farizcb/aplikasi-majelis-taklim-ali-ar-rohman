<?php

include('connect.php');

$postdata = file_get_contents("php://input");
$request = json_decode($postdata);

$email = $request->email;
$kode = $request->kode;

$query = "SELECT * FROM t_forgot_password WHERE email = '$email' AND kode_verifikasi = '$kode' AND status = 'AVAILABLE'";
$result = mysqli_query($conn, $query);
if (!$result) {
    printf("Error: %s\n", mysqli_error($conn));
    exit();
}

if (mysqli_num_rows($result) != 0) {
    $row = mysqli_fetch_assoc($result);
    $id = $row["id"];
    $query_update = "UPDATE t_forgot_password set status = 'EXPIRED' WHERE id = '$id'";
    mysqli_query($conn, $query_update);
    $response = array('data' => null, 'status' => 'SUCCESS', 'message' => 'Verifikasi data berhasil, silahkan ubah password !');
} else {
    $response = array('data' => null, 'status' => 'ERROR', 'message' => 'Email atau kode verifikasi tidak cocok !');
}

header('Content-Type: application/json');
echo json_encode($response);