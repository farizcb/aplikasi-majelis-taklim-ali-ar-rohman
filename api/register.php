<?php

include('connect.php');

$postdata = file_get_contents("php://input");
$request = json_decode($postdata);

$username = $request->username;
$email = $request->email;
$nama = $request->namaLengkap;
$password = $request->password;
$hash = password_hash($password, PASSWORD_BCRYPT);
$status = 'PENDING';
$role = 'MURID';

$query = "INSERT INTO c_security_user VALUES(NULL, '$username', '$email', '$nama', '$hash', '$status', '$role')";

$result = mysqli_query($conn, $query);

if ($result) {
	$response = array('data' => null, 'status' => 'SUCCESS', 'message' => 'Pendaftaran berhasil !');
} else {
	$response = array('data' => null, 'status' => 'ERROR', 'message' => 'Gagal menambahkan data ke database');
}

header('Content-Type: application/json');
echo json_encode($response);
