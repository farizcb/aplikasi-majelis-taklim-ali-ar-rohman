<?php
include('connect.php');

$id = $_GET["id"];

$query = "DELETE FROM m_kegiatan WHERE id = '$id'";
$result = mysqli_query($conn, $query);

if ($result) {
    $response = array('data' => null, 'status' => 'SUCCESS', 'message' => 'Data berhasil dihapus');
} else {
    $response = array('data' => null, 'status' => 'ERROR', 'message' => 'Gagal menambahkan data ke database');
}

header('Content-Type: application/json');
echo json_encode($response);
