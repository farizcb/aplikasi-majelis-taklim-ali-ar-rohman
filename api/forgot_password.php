<?php
include('connect.php');

$postdata = file_get_contents("php://input");
$request = json_decode($postdata);

$email = $request->email;

$query = "SELECT * FROM c_security_user WHERE email = '$email'";
$result = mysqli_query($conn, $query);

if (!$result) {
    printf("Error: %s\n", mysqli_error($conn));
    exit();
}

if (mysqli_num_rows($result) != 0) {
    $dataUser = mysqli_fetch_assoc($result);
    $kode = rand(0, 999999);

    $query_input_forgot_password = "INSERT INTO t_forgot_password VALUES(NULL, '$email', '$kode', 'AVAILABLE')";
    $result_insert = mysqli_query($conn, $query_input_forgot_password);

    if ($result_insert) {
        $last_id = mysqli_insert_id($conn);
        $query_find_forgot_password_not_id = "SELECT * FROM t_forgot_password WHERE id <> '$last_id'";
        $result_find = mysqli_query($conn, $query_find_forgot_password_not_id);
        if (mysqli_num_rows($result_find) != 0) {
            while ($row = mysqli_fetch_assoc($result_find)) {
                $id = $row["id"];
                $query_update = "UPDATE t_forgot_password SET status = 'EXPIRED' where id = '$id'";
                mysqli_query($conn, $query_update);
            }
        }
        $from = "dev.cb.email@gmail.com";
        $to = $email;
        $subject = "Lupa Password - Majelis Ta'lim Ali Arrohman Apps";
        $message = "Kode verifikasi : " . $kode;
        $headers = "From: " . $from;

        if (mail($to, $subject, $message, $headers)) {
            $response = array('data' => null, 'status' => 'SUCCESS', 'message' => 'Kode verifikasi sudah dikirim, silahkan cek email Anda !');
        } else {
            $response = array('data' => null, 'status' => 'ERROR', 'message' => 'Gagal mengirim email !');
        }
    } else {
        $response = array('data' => null, 'status' => 'ERROR', 'message' => 'Gagal mengirimkan data ke database !');
    }
} else {
    $response = array('data' => null, 'status' => 'ERROR', 'message' => 'Email [' . $email . '] tidak ditemukan !');
}

header('Content-Type: application/json');
echo json_encode($response);
