<?php

include('connect.php');

$postdata = file_get_contents("php://input");
$request = json_decode($postdata);

$nama = $request->namaKegiatan;
$flagPatungan = $request->flagPatungan;
$tanggalKegiatan = date('Y-m-d', strtotime($request->tanggalKegiatan));
$tanggalDibuat = date('Y-m-d H:i:s');

$query = "INSERT INTO m_kegiatan VALUES(NULL, '$nama', '$tanggalKegiatan', '$flagPatungan', '$tanggalDibuat')";

$result = mysqli_query($conn, $query);

if ($result) {
	$response = array('data' => null, 'status' => 'SUCCESS', 'message' => 'Data berhasil disimpan !');
} else {
	$response = array('data' => null, 'status' => 'ERROR', 'message' => 'Gagal menambahkan data ke database');
}

header('Content-Type: application/json');
echo json_encode($response);