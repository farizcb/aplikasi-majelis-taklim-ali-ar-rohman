<?php
session_start();
include('connect.php');

$postdata = file_get_contents("php://input");
$request = json_decode($postdata);

$username = $request->username;
$password = $request->password;

$query = "SELECT * FROM c_security_user WHERE (username = '$username' OR email = '$username') AND status = 'VERIFIED'";
$result = mysqli_query($conn, $query);

if (!$result) {
    printf("Error: %s\n", mysqli_error($conn));
    exit();
}

if (mysqli_num_rows($result) != 0) {
    $dataUser = mysqli_fetch_assoc($result);
    if (password_verify($password, $dataUser['password'])) {
        $_SESSION['username'] = $dataUser['username'];
        $_SESSION['role'] = $dataUser['role'];
        $response = array('data' => null, 'status' => 'SUCCESS', 'message' => 'Berhasil login !');
    } else {
        $response = array('data' => null, 'status' => 'ERROR', 'message' => 'Username atau password salah !');
    }
} else {
    $response = array('data' => null, 'status' => 'ERROR', 'message' => 'Username atau password salah !');
}

header('Content-Type: application/json');
echo json_encode($response);
