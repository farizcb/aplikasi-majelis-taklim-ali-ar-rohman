<?php
$serverName = "127.0.0.1";
$username = "root";
$password = "";
$dbName = "ali_arrohman";

// Create connection
$conn = new mysqli($serverName, $username, $password, $dbName);

// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

// Set timezone Asia/Bangkok
date_default_timezone_set("Asia/Bangkok");