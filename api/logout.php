<?php
session_start();

$_SESSION['username'] = '';
unset($_SESSION['username']);
unset($_SESSION['role']);
session_unset();
session_destroy();
header('location:../view/auth-login.php');
