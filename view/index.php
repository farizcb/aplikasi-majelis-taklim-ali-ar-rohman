<?php
session_start();

if (!isset($_SESSION['username'])) {
    header('location:auth-login.php');
} else {
?>
    <!DOCTYPE html>
    <html class="loading" lang="en" data-textdirection="ltr">
    <!-- BEGIN: Head-->

    <head>
        <?php include('global-head.php');  ?>
    </head>
    <!-- END: Head-->

    <!-- BEGIN: Body-->

    <body class="vertical-layout vertical-menu-modern dark-layout 2-columns  navbar-floating footer-static  " data-open="click" data-menu="vertical-menu-modern" data-col="2-columns" data-layout="dark-layout">
        <!-- BEGIN: Header-->
        <?php include('header.php'); ?>
        <!-- END: Header-->

        <!-- BEGIN: Main Menu-->
        <?php include('sidebar.php'); ?>
        <!-- END: Main Menu-->

        <!-- BEGIN: Content-->
        <div class="app-content content">
            <div class="content-overlay"></div>
            <div class="header-navbar-shadow"></div>
            <div class="content-wrapper">
                <div class="content-header row">
                </div>
                <div class="content-body">
                    <!-- Dashboard Analytics Start -->
                    <section id="dashboard-analytics">

                    </section>
                    <!-- Dashboard Analytics end -->

                </div>
            </div>
        </div>
        <!-- END: Content-->

        <div class="sidenav-overlay"></div>
        <div class="drag-target"></div>

        <!-- BEGIN: Footer-->
        <?php include('footer.php'); ?>
        <!-- END: Footer-->

        <!-- BEGIN: Javascript-->
        <?php include('javascript.php'); ?>
        <!-- END: Javascript-->

    </body>
    <!-- END: Body-->

    </html>
<?php
}
?>