<?php
$actual_link = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
?>
<div class="main-menu menu-fixed menu-light menu-accordion menu-shadow" data-scroll-to-active="true">
    <div class="navbar-header">
        <ul class="nav navbar-nav flex-row">
            <li class="nav-item mr-auto"><a class="navbar-brand" href="index.php">
                    <div class="brand-logo"></div>
                    <h2 class="brand-text mb-0">Ali Arrohman</h2>
                </a></li>
            <li class="nav-item nav-toggle"><a class="nav-link modern-nav-toggle pr-0" data-toggle="collapse"><i class="feather icon-x d-block d-xl-none font-medium-4 primary toggle-icon"></i><i class="toggle-icon feather icon-disc font-medium-4 d-none d-xl-block collapse-toggle-icon primary" data-ticon="icon-disc"></i></a></li>
        </ul>
    </div>
    <div class="shadow-bottom"></div>
    <div class="main-menu-content">
        <ul class="navigation navigation-main" id="main-menu-navigation" data-menu="menu-navigation">
            <li class="nav-item <?php echo (strpos($actual_link, "index.php")) ? 'active' : ''; ?>">
                <a href="index.php"><i class="feather icon-home"></i><span class="menu-title" data-i18n="Beranda">Beranda</span></a>
            </li>
            <li class=" navigation-header">
                <span>Apps</span>
            </li>
            <li class="nav-item <?php echo (strpos($actual_link, "kegiatan.php")) ? 'active' : ''; ?>">
                <a href="../view/kegiatan.php"><i class="feather icon-calendar"></i><span class="menu-title" data-i18n="Kegiatan">Kegiatan</span></a>
            </li>
            <li class="nav-item">
                <a href="../api/logout.php"><i class="feather icon-power"></i><span class="menu-title" data-i18n="Logout">Logout</span></a>
            </li>
        </ul>
    </div>
</div>