<?php
session_start();

if (!isset($_SESSION['username'])) {
    header('location:auth-login.php');
} else {
?>
    <!DOCTYPE html>
    <html class="loading" lang="en" data-textdirection="ltr">
    <!-- BEGIN: Head-->

    <head>
        <?php include('global-head.php') ?>

        <link rel="stylesheet" type="text/css" href="../app-assets/vendors/css/tables/datatable/datatables.min.css">
        <link rel="stylesheet" type="text/css" href="../app-assets/vendors/css/tables/datatable/extensions/dataTables.checkboxes.css">
        <link rel="stylesheet" type="text/css" href="../app-assets/css/pages/data-list-view.min.css">
        <link rel="stylesheet" type="text/css" href="../app-assets/vendors/css/pickers/pickadate/pickadate.css">
        <link rel="stylesheet" type="text/css" href="../app-assets/vendors/css/animate/animate.css">
        <link rel="stylesheet" type="text/css" href="../app-assets/vendors/css/extensions/sweetalert2.min.css">
    </head>
    <!-- END: Head-->

    <!-- BEGIN: Body-->

    <body class="vertical-layout vertical-menu-modern dark-layout 2-columns  navbar-floating footer-static  " data-open="click" data-menu="vertical-menu-modern" data-col="2-columns" data-layout="dark-layout">

        <!-- BEGIN: Header-->
        <?php include('header.php'); ?>
        <!-- END: Header-->

        <!-- BEGIN: Main Menu-->
        <?php include('sidebar.php'); ?>
        <!-- END: Main Menu-->

        <!-- BEGIN: Content-->
        <div class="app-content content" id="app">
            <div class="content-overlay"></div>
            <div class="header-navbar-shadow"></div>
            <div class="content-wrapper">
                <div class="content-body">
                    <section id="table-kegiatan" class="data-list-view-header">
                        <div class="table-responsive">
                            <div class="dataTables_wrapper dt-bootstrap4" id="divTambah">
                                <div class="top">
                                    <div class="actions action-btns">
                                        <div class="dt-buttons btn-group">
                                            <button type="button" class="btn btn-outline-primary" @click="showFormTambah()">
                                                <span><i class="feather icon-plus"></i> Tambah</span>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <table class="table table-kegiatan">
                                <thead>
                                    <tr>
                                        <th>Nama Kegiatan</th>
                                        <th>Butuh Patungan</th>
                                        <th>Tanggal Kegiatan</th>
                                        <th>Detail</th>
                                        <th>Aksi</th>
                                    </tr>
                                </thead>
                                <tbody id="table-body"></tbody>
                                <tfoot>
                                    <tr>
                                        <th>Nama Kegiatan</th>
                                        <th>Butuh Patungan</th>
                                        <th>Tanggal Kegiatan</th>
                                        <th>Detail</th>
                                        <th>Aksi</th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                        <!-- add new sidebar starts -->
                        <div class="add-new-data-sidebar">
                            <div class="overlay-bg"></div>
                            <div class="add-new-data">
                                <div class="div mt-2 px-2 d-flex new-data-title justify-content-between">
                                    <div>
                                        <h4 class="text-uppercase" id="header-add">Tambah Kegiatan</h4>
                                    </div>
                                    <div class="hide-data-sidebar">
                                        <i class="feather icon-x"></i>
                                    </div>
                                </div>
                                <div class="data-items pb-3">
                                    <div class="data-fields px-2 mt-3">
                                        <div class="row">
                                            <div class="col-sm-12 data-field-col">
                                                <label for="data-name">Nama Kegiatan</label>
                                                <input type="text" class="form-control data-name" v-model="namaKegiatan">
                                            </div>
                                            <div class="col-sm-12 data-field-col">
                                                <label>Butuh Patungan</label>
                                                <div class="vs-radio-con vs-radio-success">
                                                    <input type="radio" name="flagPatungan" value="Ya" v-model="flagPatungan">
                                                    <span class="vs-radio">
                                                        <span class="vs-radio--border"></span>
                                                        <span class="vs-radio--circle"></span>
                                                    </span>
                                                    <span class="">Ya</span>
                                                </div>
                                                <div class="vs-radio-con vs-radio-danger">
                                                    <input type="radio" name="flagPatungan" value="Tidak" v-model="flagPatungan">
                                                    <span class="vs-radio">
                                                        <span class="vs-radio--border"></span>
                                                        <span class="vs-radio--circle"></span>
                                                    </span>
                                                    <span class="">Tidak</span>
                                                </div>
                                            </div>
                                            <div class="col-sm-12 data-field-col">
                                                <label for="data-tanggal-kegiatan">Tanggal Kegiatan</label>
                                                <input type='text' class="form-control" id="tanggal-kegiatan-picker" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="add-data-footer d-flex justify-content-around px-3 mt-2">
                                    <div class="add-data-btn">
                                        <button type="button" class="btn btn-primary" @click="simpan()">Simpan</button>
                                    </div>
                                    <div class="cancel-data-btn">
                                        <button class="btn btn-outline-danger">Batal</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- add new sidebar ends -->
                    </section>
                </div>
            </div>
        </div>
        <!-- END: Content-->

        <div class="sidenav-overlay"></div>
        <div class="drag-target"></div>

        <!-- BEGIN: Footer-->
        <?php include('footer.php'); ?>
        <!-- END: Footer-->

        <!-- BEGIN: Javascript-->
        <?php include('javascript.php'); ?>

        <script src="../app-assets/vendors/js/tables/datatable/datatables.min.js"></script>
        <script src="../app-assets/vendors/js/tables/datatable/datatables.buttons.min.js"></script>
        <script src="../app-assets/vendors/js/tables/datatable/datatables.bootstrap4.min.js"></script>
        <script src="../app-assets/vendors/js/tables/datatable/buttons.bootstrap.min.js"></script>
        <script src="../app-assets/vendors/js/tables/datatable/dataTables.select.min.js"></script>
        <script src="../app-assets/vendors/js/tables/datatable/datatables.checkboxes.min.js"></script>
        <script src="../app-assets/vendors/js/pickers/pickadate/picker.js"></script>
        <script src="../app-assets/vendors/js/pickers/pickadate/picker.date.js"></script>
        <script src="../app-assets/vendors/js/pickers/pickadate/picker.time.js"></script>
        <script src="../app-assets/vendors/js/pickers/pickadate/legacy.js"></script>
        <script src="../app-assets/vendors/js/extensions/sweetalert2.all.min.js"></script>
        <script src="../app-assets/js/scripts/ui/kegiatan.js"></script>
        <script src="../app-assets/js/scripts/util/util.js"></script>
        <!-- END: Javascript-->

        <!-- BEGIN: Vue JS -->
        <script src="https://unpkg.com/axios/dist/axios.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/vue@2.6.12"></script>
        <script>
            function confirmDelete(id) {
                Swal.fire({
                    title: 'Apakah Anda yakin ?',
                    text: "Data yang sudah dihapus tidak dapat dikembalikan",
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    cancelButtonText: 'Batal',
                    confirmButtonText: 'Ya, hapus',
                    confirmButtonClass: 'btn btn-primary',
                    cancelButtonClass: 'btn btn-danger ml-1',
                    buttonsStyling: false,
                }).then(function(result) {
                    if (result.value) {
                        axios.get('../api/delete_kegiatan.php?id=' + id)
                            .then((response) => {
                                if (response.data.status === "SUCCESS") {
                                    Swal.fire({
                                        type: "success",
                                        title: 'Berhasil',
                                        text: response.data.message,
                                        confirmButtonClass: 'btn btn-success',
                                    }).then(function(result1) {
                                        if (result1.value) {
                                            location.replace("kegiatan.php")
                                        }
                                    })
                                }
                            })
                    }
                })
            }

            const vm = new Vue({
                el: "#app",
                data: {
                    id: '',
                    namaKegiatan: '',
                    flagPatungan: 'Ya',
                    tanggalKegiatan: ''
                },
                methods: {
                    simpan: function() {
                        let objReq = {
                            id: this.id,
                            namaKegiatan: this.namaKegiatan,
                            flagPatungan: this.flagPatungan,
                            tanggalKegiatan: $('#tanggal-kegiatan-picker').val()
                        }
                        if (!objReq.namaKegiatan) {
                            this.showMessageError("Nama kegiatan tidak boleh kosong !")
                            return
                        } else if (!objReq.tanggalKegiatan) {
                            this.showMessageError("Tanggal kegiatan tidak boleh kosong !")
                            return
                        }
                        console.log(objReq);
                        if (objReq.id) {
                            axios.post('../api/edit_kegiatan.php', objReq)
                                .then((response) => {
                                    if (response.data.status === "SUCCESS") {
                                        Swal.fire({
                                            title: "Success !",
                                            text: response.data.message,
                                            type: "success",
                                            confirmButtonClass: 'btn btn-primary',
                                            buttonsStyling: false,
                                        }).then(function(result) {
                                            if (result.value) {
                                                location.replace("kegiatan.php")
                                            }
                                        })
                                    } else {
                                        this.showMessageError(response.data.message)
                                    }
                                })
                        } else {
                            axios.post('../api/save_kegiatan.php', objReq)
                                .then((response) => {
                                    if (response.data.status === "SUCCESS") {
                                        Swal.fire({
                                            title: "Success !",
                                            text: response.data.message,
                                            type: "success",
                                            confirmButtonClass: 'btn btn-primary',
                                            buttonsStyling: false,
                                        }).then(function(result) {
                                            if (result.value) {
                                                location.replace("kegiatan.php")
                                            }
                                        })
                                    } else {
                                        this.showMessageError(response.data.message)
                                    }
                                })
                        }
                    },
                    findAll: function() {
                        axios.get('../api/find_all_kegiatan.php')
                            .then((response) => {
                                let bodyTable = "";
                                if (response.data) {
                                    for (let i = 0; i < response.data.length; i++) {
                                        bodyTable += "<tr'>\n\
                                            <td>" + response.data[i].nama + "</td>\n\
                                            <td>" + response.data[i].flag_patungan + "</td>\n\
                                            <td>" + response.data[i].tanggal_kegiatan + "</td>\n\
                                            <td>";
                                        if (response.data[i].flag_patungan === 'Ya') {
                                            bodyTable += "<span class='btn btn-primary' onclick='confirmDelete(" + response.data[i].id + ")'>Detail</span>";
                                        } else {
                                            bodyTable += "-";
                                        }

                                        bodyTable += "</td>\n\
                                            <td style='cursor: pointer;'>\n\
                                                <span class='action-edit' onclick='edit(" + response.data[i].id + ")'><i class='feather icon-edit'></i></span>\n\
                                                <span class='action-delete' onclick='confirmDelete(" + response.data[i].id + ")'><i class='feather icon-trash'></i></span>\n\
                                            </td>\n\
                                        </tr>";
                                    }
                                } else {
                                    bodyTable += "<tr style='text-align: center;'><td colspan='5'>Data tidak ditemukan</td></tr>";
                                }
                                $("#table-body").html(bodyTable);
                                if (response.data) {
                                    this.defaultTable();
                                    $("#divTambah").hide();
                                } else {
                                    $("#divTambah").show();
                                }
                            })
                    },
                    showMessageError: function(message) {
                        Swal.fire({
                            title: "Error !",
                            text: message,
                            type: "error",
                            confirmButtonClass: 'btn btn-primary',
                            buttonsStyling: false,
                        });
                    },
                    defaultTable: function() {
                        var dataListView = $(".table-kegiatan").DataTable({
                            responsive: false,
                            columnDefs: [{
                                targets: 0
                            }],
                            dom: '<"top"<"actions action-btns"B><"action-filters"lf>><"clear">rt<"bottom"<"actions">p>',
                            oLanguage: {
                                sLengthMenu: "_MENU_",
                                sSearch: ""
                            },
                            aLengthMenu: [
                                [5, 10, 50, 100],
                                [5, 10, 50, 100]
                            ],
                            pageLength: 5,
                            buttons: [{
                                text: "<i class='feather icon-plus'></i> Tambah",
                                action: function() {
                                    $(this).removeClass("btn-secondary")
                                    $(".add-new-data").addClass("show")
                                    $(".overlay-bg").addClass("show")
                                    $(".data-name, #tanggal-kegiatan-picker").val("")
                                    $("#header-add").text("Tambah Kegiatan")
                                    vm.$data.id = null;
                                },
                                className: "btn-outline-primary"
                            }],
                            initComplete: function(settings, json) {
                                $(".dt-buttons .btn").removeClass("btn-secondary")
                            }
                        });
                    },
                    showFormTambah: function() {
                        $(this).removeClass("btn-secondary")
                        $(".add-new-data").addClass("show")
                        $(".overlay-bg").addClass("show")
                        $(".data-name, #tanggal-kegiatan-picker").val("")
                        $("#header-add").text("Tambah Kegiatan")
                    }
                },
                created() {
                    this.findAll()
                }
            })

            function edit(id) {
                $(this).removeClass("btn-secondary")
                $(".add-new-data").addClass("show")
                $(".overlay-bg").addClass("show")
                $("#header-add").text("Ubah Kegiatan");
                axios.get('../api/find_by_id_kegiatan.php?id=' + id)
                    .then((response) => {
                        if (response.data) {
                            vm.$data.id = response.data.id;
                            vm.$data.namaKegiatan = response.data.nama;
                            vm.$data.flagPatungan = response.data.flag_patungan;
                            $("#tanggal-kegiatan-picker").val(formatDate(response.data.tanggal_kegiatan));
                        }
                    })
            }
        </script>
        <!-- END: Vue JS -->

    </body>
    <!-- END: Body-->

    </html>
<?php
}
?>