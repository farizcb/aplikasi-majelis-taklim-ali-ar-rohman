<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
<meta name="author" content="Muhammad Fariz">
<title>Ali Arrohman</title>
<link rel="shortcut icon" type="image/x-icon" href="../app-assets/images/ico/favicon.ico">
<link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,500,600" rel="stylesheet">

<!-- BEGIN: Vendor CSS-->
<link rel="stylesheet" type="text/css" href="../app-assets/vendors/css/vendors.min.css">
<!-- END: Vendor CSS-->

<!-- BEGIN: Theme CSS-->
<link rel="stylesheet" type="text/css" href="../app-assets/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="../app-assets/css/bootstrap-extended.min.css">
<link rel="stylesheet" type="text/css" href="../app-assets/css/colors.min.css">
<link rel="stylesheet" type="text/css" href="../app-assets/css/components.min.css">
<link rel="stylesheet" type="text/css" href="../app-assets/css/themes/dark-layout.min.css">
<link rel="stylesheet" type="text/css" href="../app-assets/css/themes/semi-dark-layout.min.css">

<!-- BEGIN: Page CSS-->
<link rel="stylesheet" type="text/css" href="../app-assets/css/core/menu/menu-types/vertical-menu.min.css">
<link rel="stylesheet" type="text/css" href="../app-assets/css/core/colors/palette-gradient.min.css">
<!-- END: Page CSS-->

<!-- BEGIN: Custom CSS-->
<link rel="stylesheet" type="text/css" href="../assets/css/style.css">
<!-- END: Custom CSS-->